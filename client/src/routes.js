import React, { Fragment, lazy, Suspense } from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';
import Helmet from 'react-helmet';

import Main from './containers/main';
import Header from './containers/header';
import Admin from './containers/admin';

// import Footer from './components/footer';

const Routes = () => (
  <Fragment>
    <Route component={Header} />
    <Switch>
      <Route exact path='/' component={Main} />
      <Route exact path='/admin' component={Admin} />
    </Switch>
  </Fragment>
);

export default Routes;
