import React, { Fragment, Component, useLayoutEffect } from 'react';
import './landing.scss';
import axios from 'axios';
import { connect } from 'react-redux';
import { YMaps, Map, Placemark } from 'react-yandex-maps';

import { addCart, removeCart } from '../../reducers/clientReducer';

const mapData = {
  center: [43.233386, 76.960126],
  zoom: 17,
};

const coordinates = [[43.233386, 76.960126]];

class Main extends Component {
  constructor(props) {
    super(props);

    this.state = {
      flowers: [],
      slice: 6,
      more: true,
      modalIsOpen: false,
      modalFlower: null,
    };

    this.openModal = this.openModal.bind(this);
    this.closeModal = this.closeModal.bind(this);
  }

  componentDidMount() {
    axios
      .get(`/api/flowers/`)
      .then((response) => {
        console.log(response.data);
        this.setState({
          flowers: response.data,
        });
      })
      .catch((err) => {});
  }

  sliceOpen() {
    this.setState({
      slice: this.state.flowers.lendth,
      more: false,
    });
  }

  closeModal() {
    this.setState({
      modalFlower: null,
    });
  }

  openModal(i) {
    this.setState({
      modalFlower: i,
    });
  }

  addToCart(i) {
    this.props.addCart(i);
  }

  removeFromCart(i) {
    this.props.removeCart(i);
  }

  sliceClose() {
    this.setState({
      slice: 6,
      more: true,
    });
  }

  render() {
    console.log(this.props);

    return (
      <Fragment>
        {this.state.modalFlower ? (
          <div className='l-modal'>
            <div className='l-modal-bg'></div>
            <div className='l-modal-inner'>
              <div className='l-modal-close'>
                <img
                  onClick={this.closeModal}
                  src='/images/icons/close.svg'
                  alt=''
                />
              </div>
              <div className='l-modal-content'>
                <div
                  className='l-modal-img'
                  style={{
                    backgroundImage: `url(${this.state.modalFlower.image})`,
                  }}
                ></div>
                <div className='l-modal-info'>
                  <div className='l-modal-info-inner'>
                    <div className='l-modal-info-title'>
                      {this.state.modalFlower.name}
                    </div>
                    <div className='l-modal-info-desc'>
                      {this.state.modalFlower.desc}
                    </div>
                    <div className='l-modal-info-price'>
                      {this.state.modalFlower.price} тнг
                    </div>
                  </div>
                  {this.props.client.cart.find(
                    (obj) => obj.name === this.state.modalFlower.name,
                  ) ? (
                    <div className='l-modal-info-button-active'>
                      <span
                        onClick={() =>
                          this.removeFromCart(this.state.modalFlower)
                        }
                      >
                        В корзине
                      </span>
                    </div>
                  ) : (
                    <div className='l-modal-info-button'>
                      <span
                        onClick={() => this.addToCart(this.state.modalFlower)}
                      >
                        В корзину
                      </span>
                    </div>
                  )}
                </div>
              </div>
            </div>
          </div>
        ) : null}

        <div className='l'>
          <div className='l__inn'>
            <div className='l__inn-welcome'>
              <div className='container'>
                <div className='l__inn-welcome-content'>
                  <div className='l__inn-welcome-title'>
                    RR Flowers доставка цветов в Алматы
                  </div>
                  <div className='l__inn-welcome-text'>
                    магазин композиций из живых и искусственных цветов, корзин,
                    ваз, горшков и других атрибутов интерьера.
                  </div>
                </div>
              </div>
            </div>

            <div className='l__inn-services'>
              <div className='container'>
                <div className='l__inn-services-items'>
                  <div className='l__inn-services-items-i'>
                    <div className='l__inn-services-items-i-img'>
                      <img src='../images/icons/bouqet.svg' alt='' />
                    </div>
                    <div className='l__inn-services-items-i-text'>
                      Готовые букеты от Флористов
                    </div>
                  </div>
                  <div className='l__inn-services-items-i'>
                    <div className='l__inn-services-items-i-img'>
                      <img src='../images/icons/stopwatch.svg' alt='' />
                    </div>
                    <div className='l__inn-services-items-i-text'>
                      Доставка по городу Алматы
                    </div>
                  </div>
                  <div className='l__inn-services-items-i'>
                    <div className='l__inn-services-items-i-img'>
                      <img src='../images/icons/valentines.svg' alt='' />
                    </div>
                    <div className='l__inn-services-items-i-text'>
                      Открытка с ручной напдписью в подарок
                    </div>
                  </div>
                  <div className='l__inn-services-items-i'>
                    <div className='l__inn-services-items-i-img'>
                      <img src='../images/icons/tux.svg' alt='' />
                    </div>
                    <div className='l__inn-services-items-i-text'>
                      Мы работаем без пауыз и выходных
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <div className='l__inn-catalog'>
              <div className='container'>
                <div className='l__inn-catalog-content'>
                  <h1 className='l__inn-catalog__text'>Наши букеты</h1>
                  {/* <div className='l__inn-catalog__title'>
                                        <span>Новинки</span>
                                        <span>По повадам <br/> скоро!</span>
                                    </div> */}
                  <div className='l__inn-catalog__items'>
                    {this.state.flowers
                      .slice(0, this.state.slice)
                      .map((i, k) => {
                        return (
                          <div className='l__inn-catalog__items-i' key={k}>
                            <div
                              className='l__inn-catalog__items-i-img'
                              style={{ backgroundImage: `url(${i.image})` }}
                              onClick={() => this.openModal(i)}
                            >
                              <div className='l__inn-catalog__items-i-img-inner'>
                                <span>Подробнее</span>
                              </div>
                            </div>
                            <div className='l__inn-catalog__items-i-inner'>
                              <div className='l__inn-catalog__items-i-title'>
                                {i.name}
                              </div>
                              <div className='l__inn-catalog__items-i-about'>
                                {i.desc}
                              </div>
                              <div className='l__inn-catalog__items-i-price'>
                                <div className='l__inn-catalog__items-i-price-count'>
                                  {i.price} тг
                                </div>
                                {this.props.client.cart.find(
                                  (obj) => obj.name === i.name,
                                ) ? (
                                  <div
                                    className='l__inn-catalog__items-i-price-cart l__inn-catalog__items-i-price-cart-active'
                                    onClick={() => this.removeFromCart(i)}
                                  >
                                    В корзинe
                                  </div>
                                ) : (
                                  <div
                                    className='l__inn-catalog__items-i-price-cart '
                                    onClick={() => this.addToCart(i)}
                                  >
                                    В корзину
                                  </div>
                                )}
                              </div>
                            </div>
                          </div>
                        );
                      })}
                  </div>
                  <div className='l__inn-catalog__more'>
                    {this.state.more ? (
                      <span onClick={() => this.sliceOpen()}>Показать еще</span>
                    ) : (
                      <span onClick={() => this.sliceClose()}>Cкрыть</span>
                    )}
                  </div>
                </div>
              </div>
            </div>

            <img className='l__inn-div' src='../images/div.png' alt='' />

            <div className='l__inn-about'>
              <div className='container'>
                <div className='l__inn-about-content'>
                  <div className='l__inn-about-inner'>
                    <div className='l__inn-about-text'>
                      <div className='l__inn-about-title'>
                        <span>О нас</span>
                      </div>
                      За минувшие 2 года мы оформили цветами несколько десятков
                      свадеб, дней рождения и корпоративных мероприятий и
                      создали тысячи букетов! <br />
                      К вашим услугам 3 флориста, которых мы с гордостью
                      называем цветочными мастерами, потому что они
                      по-настоящему влюблены в свою профессию и за годы работы
                      стали настоящими мастерами своего дела. <br />
                      Загляните к нам, вы окажетесь в атмосфере праздника цветов
                      с самого первого шага. У нас вы найдете не только цветы,
                      но и множество интересных принадлежностей для цветочного
                      декора. <br />
                    </div>
                    <div className='l__inn-about-img'>
                      <img src='../images/back/about.jpg' alt='' />
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className='l__inn-ind'>
              <div className='container'>
                <div className='l__inn-ind-content'>
                  <div className='l__inn-ind-img'>
                    <img src='../images/back/ind.jpg' alt='' />
                  </div>
                  <div className='l__inn-ind-text'>
                    <h1>Не нашли подходящий букет</h1>
                    <h3>
                      Соберем подходящий букет под ваши нужды, <br /> поможем
                      подобрать декор и аксессуары
                    </h3>
                    <div className='l__inn-ind-text-button'>
                      <a
                        href='https://api.whatsapp.com/send?phone=87083761466&text=%D0%97%D0%B4%D1%80%D0%B0%D0%B2%D1%81%D1%82%D0%B2%D1%83%D0%B9%D1%82%D0%B5,%20%D0%AF%20%D1%85%D0%BE%D1%82%D0%B5%D0%BB%20%D0%B1%D1%8B%20%D0%BF%D0%BE%D0%B4%D0%BE%D0%B1%D1%80%D0%B0%D1%82%D1%8C%20%D0%B1%D1%83%D0%BA%D0%B5%D1%82%20%D0%B2%D0%BC%D0%B5%D1%81%D1%82%D0%B5%20%D1%81%20%D1%84%D0%BB%D0%BE%D1%80%D0%B8%D1%81%D1%82%D0%BE%D0%BC'
                        target='_blank'
                      >
                        Подобрать букет
                      </a>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <img className='l__inn-div' src='../images/div.png' alt='' />

            <div className='l__inn-footer'>
              <div className='container'>
                <div className='l__inn-footer-content'>
                  <div className='l__inn-footer-map'>
                    <YMaps className='l__inn-footer-map-inn'>
                      <Map defaultState={mapData} width={'100%'} height={320}>
                        {coordinates.map((coordinate) => (
                          <Placemark
                            geometry={coordinate}
                            options={{
                              iconColor: '#00c4cc',
                              presetStorage: 'islands#circleIcon',
                            }}
                          />
                        ))}
                      </Map>
                    </YMaps>
                  </div>
                  <div className='l__inn-footer-contacts'>
                    <div className='l__inn-footer-contacts-address'>
                      <h3>Адрес:</h3>
                      <p>г.Алматы,ул.Ньютона, дом 1</p>
                    </div>
                    <div className='l__inn-footer-contacts-row'>
                      <div className='l__inn-footer-contacts-row-i'>
                        <h3>Время работы:</h3>
                        <p>24/7</p>
                      </div>
                    </div>
                    <div className='l__inn-footer-contacts-row'>
                      <div className='l__inn-footer-contacts-row-i'>
                        <h3>Телефоны :</h3>
                        <p>+7 708 376 1466</p>
                      </div>
                      <div className='l__inn-footer-contacts-row-i'>
                        <h3>instagram:</h3>
                        <p>@rr_gifts_flowers</p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </Fragment>
    );
  }
}

const mapStateToProps = (state) => ({
  client: state.client,
});

export default connect(mapStateToProps, {
  addCart,
  removeCart,
})(Main);
