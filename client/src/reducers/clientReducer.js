import axios from 'axios';

const ADD_CART = 'ADD_CART';
const REMOVE_CART = 'REMOVE_CART';
const PROCEED = 'PROCEED';
const PROCEED_ERR = 'PROCEED_ERR';


const initialState = {
    loading: 0,
    cart: [],
    price: 0,
    proceed:false,
    proceedErr:false
};

export default function (state = initialState, action) {
    switch (action.type) {
        case ADD_CART:
            return {
                ...state,
                cart: [...state.cart, action.payload],
                price: state.price + action.payload.price
            };
        case REMOVE_CART:
            return {
                ...state,
                cart: state.cart.filter(fl => fl.name !== action.payload.name),
                price: state.price - action.payload.price
            };
        case PROCEED:
            return {
                ...state,
                proceed: true,
            };
        case PROCEED_ERR:
            return {
                ...state,
                proceedErr: true,
            };
        default:
            return state;
    }
}

export const addCart = (props) => dispatch => {
    console.log(props);
    dispatch({
        type: ADD_CART,
        payload: props,
    });
};

export const removeCart = (props) => dispatch => {
    console.log(props);
    dispatch({
        type: REMOVE_CART,
        payload: props,
    });
};

export const processBuy = (cart, price, name, phone ,adr,toys,candy) => dispatch => {
    axios
        .post(`/api/order/`, {
            name: name,
            price: price,
            phone: phone,
            cart: cart,
            address: adr,
            toys: toys,
            candy: candy,
        })
        .then(response => {
            dispatch({
                type:PROCEED
            })
        })
        .catch(err => {
            dispatch({
                type:PROCEED_ERR
            })
        });
};