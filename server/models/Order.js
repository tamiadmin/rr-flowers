var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Order = new mongoose.Schema({
    name: String,
    phone: String,
    flowers: [],
    address: String,
    price: Number,
    toys: Boolean,
    candy: Boolean
});

module.exports = mongoose.model('Order', Order);
