var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Slide = new mongoose.Schema({
  name: String,
  desc: String,
  image: String,
});

module.exports = mongoose.model('Slide', Slide);
