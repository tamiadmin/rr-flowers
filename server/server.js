const express = require('express');
const bodyParser = require('body-parser');
const path = require('path');
var mongoose = require('mongoose');
var multer = require('multer');
mongoose.connect(
  'mongodb://stas:stas12345@ds039447.mlab.com:39447/about-flowers',
);
var upload = multer({ dest: './client/public/images/' });
var Order = require('./models/Order.js');
var Flower = require('./models/Flower.js');

const app = express();
const port = process.env.PORT || 3000;

app.use(bodyParser.json({ limit: '50mb' }));
app.use(bodyParser.urlencoded({ limit: '50mb', extended: true }));

app.use(
  require('prerender-node').set('prerenderToken', '0ZMJRJNi2tTlvdro6Cyz'),
);

// require('./config/passport')(passport);

app.use(express.static('client/public/'));
app.use('/images/', express.static('../client/images'));
app.use('/fonts/', express.static('../client/style/fonts'));

app.post('/api/order', function (req, res, next) {
  let post = new Order({
    name: req.body.name,
    phone: req.body.phone,
    flowers: req.body.cart,
    price: req.body.price,
    address: req.body.address,
    toys: req.body.toys,
    candy: req.body.candy,
  });

  post.save(function (err) {
    if (err) return res.status(400).end();
    res.send(post);
  });
});

app.get('/api/flowers', function (req, res, next) {
  Flower.find({}).exec(function (err, posts) {
    if (err) return res.status(400).end();
    res.send(posts);
  });
});

app.get('/api/orders', function (req, res, next) {
  Order.find({}).exec(function (err, posts) {
    if (err) return res.status(400).end();
    res.send(posts);
  });
});

app.get('*', (req, res) => {
  res.sendFile('index.html', {
    root: path.join(__dirname, '../client/public/'),
  });
});

app.listen(port, () => {
  console.log(`SERVER RUNNNING ON PORT ${port}`);
});

module.exports = app;
