var path = require('path');

module.exports = {
  devtool: 'source-map',
  entry: ['./client/src/index.js'],
  output: {
    path: path.join(__dirname, '/client/public/build'),
    publicPath: '/client/public/static/',
    filename: 'bundle.js',
    // chunkFilename: "[name].chunk.js"
  },
  watch:true,
  // optimization: {
  //   splitChunks: {
  //     chunks: "all",
  //     minChunks: 2
  //   }
  //   // ...some other rules
  // },
  // plugins: [
  //   new HtmlWebpackPlugin({
  //     template: "client/public/index.html",
  //     inject: true,
  //     chunks: ["app"]
  //   })
  // ],
  module: {
    rules: [
      {
        test: /\.jsx?$/,
        exclude: /(node_modules|bower_components)/,
        loader: 'babel-loader',
        query: {
          presets: ['es2015', 'react', 'stage-0'],
        },
      },
      {
        test: /\.s[ac]ss$/i,
        use: [
          // Creates `style` nodes from JS strings
          'style-loader',
          // Translates CSS into CommonJS
          'css-loader',
          // Compiles Sass to CSS
          'sass-loader',
        ],
      },
      {
        test: /\.css/,
        loaders: ['style-loader', 'css-loader'],
      },
      {
        test: /\.woff2?$|\.ttf$|\.eot$|\.svg$|\.png|\.jpe?g|\.gif$/,
        loader: 'file-loader?name=[name].[ext]',
      },
    ],
  },
};
